import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router, Switch, Route
} from "react-router-dom"
import jwt_decode from "jwt-decode"

import Catalog from "./components/Catalog"
import TopNav from "./components/layouts/Navbar"
import Auth from "./components/Auth"
import Product from "./components/Product"
import Cart from "./components/Cart"
import Transactions from "./components/Transactions"
import UserTransactions from "./components/UserTransactions"

import Register from "./components/forms/Register"
import Login from "./components/forms/Login"
import AddProduct from "./components/forms/AddProduct"

import { URL } from "./config"

function App() {
  const [products, setProducts] = useState([])
  const [user, setUser] = useState({})
  const [token, setToken] = useState("")
  
  useEffect ( () => {
    fetch(`${URL}/products`)
    .then(res => res.json())
    .then(data => {
      setProducts(data)
    })

    if(token) {
      let decoded = jwt_decode(token)
      let now = new Date()
      if(decoded.exp === now.getTime()){
        localStorage.clear()
      }
    }

    setUser(JSON.parse(localStorage.getItem("user")))
    setToken(localStorage.getItem("token"))
  }, [products, token])

  const logoutHandler = () => {
    localStorage.clear()
    setUser({})
    setToken("")
    window.location.href="/"
  }


  return (
    <Router>
      <TopNav 
        user={user} 
        token={token}
        logoutHandler={logoutHandler}
      />
      <Switch>
        <Route path="/register">
          <Register />
        </Route>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/cart">
          {
            user && token && user.isAdmin === false ?
            <Cart /> :
            <Auth />
          }
        </Route>
        <Route exact path="/transactions/:userId">
          {
            user && token && user.isAdmin === false ?
            <UserTransactions /> :
            <Auth />
          }
        </Route>
        <Router path="/transactions">
          {
            user && token && user.isAdmin ?
            <Transactions /> :
            <Auth />
          }
        </Router>
        <Route path="/add-product">
          {
            user && token && user.isAdmin ?
            <AddProduct /> :
            <Auth />
          }
        </Route>
        <Route path="/products/:id">
          <Product user={user} token={token}/>
        </Route>
        <Route path="/">
          <Catalog products={products} user={user} token={token}/>
        </Route>
      </Switch>
    </Router>
    
  )
} 

export default App;
