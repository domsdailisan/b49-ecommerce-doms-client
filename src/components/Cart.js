import React, { useState, useEffect } from "react"
import Swal from "sweetalert2"
import Stripe from "./forms/Stripe"
import { URL } from "../config"

//for STRIPE - checkout
//Publishable Key: pk_test_CilfsEsMZUN5XiGBsfdmw8jH00dWtfBVt1
//Secret Key: sk_test_xHo77hLJxKTAAyRa3smmgZ4900KdWnaS6X

const Cart = () => {
	const [cartItems, setCartItems] = useState([])
	let total =0;

	useEffect( () => {
		setCartItems(JSON.parse(localStorage.getItem('cartItems')))
	}, [])

	if(cartItems.length) {
		// let subTotals = cartItems.map(item => parseInt(item.price) * parseInt(item.quantity))

		// total = subTotals.reduce((accumulator, subPerItem) => {
		// 	return accumulator + subPerItem
		// })

		cartItems.forEach(item => {
			total += item.price * item.quantity
		})
	}

	const emptyCart = () => {
		// alert("sure?")
		localStorage.setItem('cartItems', JSON.stringify([]))
		setCartItems(JSON.parse(localStorage.getItem('cartItems')))
		window.location.href="/cart"
	}

	const deleteCartItem = (productId) => {
		let updatedCart = cartItems.filter(item => item._id !== productId)
		localStorage.setItem('cartItems', JSON.stringify(updatedCart))
		setCartItems(JSON.parse(localStorage.getItem('cartItems')))	
	}

	const quantityHandler = (quantity, productId) => {
		// alert("Quantity: " + quantity + " - " + productId)
		//ex. 25 - asuihqw76321ofewj8
		let itemToUpdate = cartItems.find(item => item._id === productId)
		itemToUpdate.quantity = parseInt(quantity)
		// console.log(itemToUpdate)
		let updatedCart = cartItems.map(item => {
			return item._id === productId ? {...item, ...itemToUpdate } : item
		})
		localStorage.setItem('cartItems', JSON.stringify(updatedCart))
	}

	const checkout = () => {
		let orders = JSON.parse(localStorage.getItem('cartItems'))
		fetch(`${URL}/transactions`, {
			method: "POST",
			body: JSON.stringify({orders, total}),
			headers: {
				"Content-Type": "application/json",
				"x-auth-token": localStorage.getItem('token')
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			Swal.fire({
				'icon': 'success',
				'title': data.message
			})
			localStorage.setItem('cartItems', JSON.stringify([]))
			setCartItems(JSON.parse(localStorage.getItem('cartItems')))
		})
	}

	return (
		<div className="col-md-10 mx-auto container">
			<h2>My Cart</h2>
			{
				cartItems.length ? 
				<table className="table table-hover text-center table-bordered">
					<thead className="thead-dark">
						<tr>
							<th>Item</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Subtotal</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						{
							cartItems.map(item => (
								<tr key={item._id}>
									<td>{item.name}</td>
									<td>{item.price}</td>
									<td>
										<input 
											type="number" 
											value={item.quantity} 
											onChange={(e) => quantityHandler(e.target.value, item._id)}
										/>
									</td>
									<td>{item.quantity * item.price}</td>
									<td>
										<button className="btn btn-danger mr-2" onClick={() => deleteCartItem(item._id)}>Delete</button>
									</td>
								</tr>
							))
						}
						<tr>
							<td colSpan="3">Total</td>
							<td colSpan="2">{total}</td>
						</tr>
						<tr>
							<td colSpan="5" className="text-center">
								<button className="btn btn-danger mr-3" onClick={emptyCart}>Empty Cart</button>
								<button className="btn btn-success mr-3" onClick={checkout}>Checkout COD</button>
								<Stripe amount={total*100} cartItems={cartItems} setCartItems={setCartItems}/>
							</td>
						</tr>
					</tbody>
				</table> :
				<h3>Cart is empty</h3>
			}
		</div>
	)
}

export default Cart