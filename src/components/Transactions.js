import React, { useState, useEffect } from "react"
import { URL } from "../config"


const Transactions = () => {
	const [transactions, setTransactions] = useState([])

	useEffect( () => {
		fetch(`${URL}/transactions`, {
			headers: {
				"x-auth-token": localStorage.getItem('token')
			}
		})
		.then(res => res.json())
		.then(data => setTransactions(data))
	})

	const updateStatus = (statusId, transactionId) => {
		let body = {
			statusId
		}

		fetch(`${URL}/transactions/${transactionId}`, {
			method: "PUT",
			body: JSON.stringify(body),
			headers: {
				"Content-Type": "application/json",
				"x-auth-token": localStorage.getItem('token')
			}
		})
		.then(res => res.json())
		.then(data => {
			alert(data.message)
		})
	}

	return (
		<div className="container">
			<h2>Transactions</h2>
			{
				transactions.length ?
				<table className="table table-hover table-bordered text-center">
					<thead className="thead-dark">
						<tr>
							<th>Transaction ID</th>
							<th>Status</th>
							<th>Amount</th>
							<th>Action/s</th>
						</tr>
					</thead>
					<tbody>
						{transactions.map(transaction => (
							<tr key={transaction._id}>
								<td>{transaction._id}</td>
								<td>
									{
										transaction.statusId === "5e93c955aa2c4a333f5d7b89" ?
										"Pending" : (transaction.statusId === "5e93c946aa2c4a333f5d7b86" ?
										"Completed" : "Cancelled"
										)
									}
								</td>
								<td>{transaction.total}</td>
								{
									transaction.statusId === "5e93c955aa2c4a333f5d7b89" ?
									<td>

										<button 
											className="btn btn-success mr-2" 
											onClick={() => updateStatus("5e93c946aa2c4a333f5d7b86", transaction._id)}
										>
											Complete
										</button>
										<button 
											className="btn btn-danger"
											onClick={() => updateStatus("5e93c96aaa2c4a333f5d7b8e", transaction._id)}
										>
											Cancel
										</button>
									</td> : null
								}
							</tr>
						))}
					</tbody>
				</table> :
				<h2>No Transactions to show</h2>
			}
		</div>
	)
}

export default Transactions