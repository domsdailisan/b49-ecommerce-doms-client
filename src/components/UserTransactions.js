import React, { useState, useEffect } from "react"
import { useParams } from "react-router-dom"
import { URL } from "../config"

const UserTransactions = () => {

	let {userId} = useParams()
	const [transactions, setTransactions] = useState([])
	useEffect( () => {
		fetch(`${URL}/transactions/${userId}`, {
			headers: {
				"x-auth-token": localStorage.getItem('token')
			}
		})
		.then(res => res.json())
		.then(data => setTransactions(data))
	})

	return (
		<div className="container">
			<h2>My Transactions</h2>
			{
				transactions.length ? 
				<table className="table table-hover table-bordered">
					<thead className="thead-dark">
						<tr>
							<th>Transaction ID</th>
							<th>Status</th>
							<th>Amount</th>
							<th>Date</th>
						</tr>
					</thead>
					<tbody>
						{transactions.map(transaction =>(
							<tr key={transaction._id}>
								<td>{transaction._id}</td>
								<td>
									{
										transaction.statusId === "5e93c955aa2c4a333f5d7b89" ?
										"Pending" : (transaction.statusId === "5e93c946aa2c4a333f5d7b86" ?
										"Completed" : "Cancelled"
										)
									}
								</td>
								<td>{transaction.total}</td>
								<td>{new Date(transaction.dateCreated).toLocaleString()}</td>
							</tr>
						))}
					</tbody>
				</table> :
				<h2>No Transactions to show</h2>
			}
		</div>
	)
}

export default UserTransactions
