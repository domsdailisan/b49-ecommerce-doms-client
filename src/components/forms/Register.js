import React, { useState } from "react"
import Swal from "sweetalert2"
import { URL } from "../../config"

const Register = () => {
	const [formData, setFormData] = useState({})
	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}

	const onSubmitHandler = (e) => {
		e.preventDefault()
		// console.log(formData)
		fetch(`${URL}/users`, {
			method: "POST",
			body: JSON.stringify(formData),
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.status === 200){
				Swal.fire({
					icon: 'success',
					text: data.message
				})
			}else {
				Swal.fire({
					icon: 'error',
					text: data.message,
					timer: 2500,
					showConfirmButton: false,
					timerProgressBar: true
				})
			}
		})
	}
	return (
		<form className="col-sm-6 mx-auto" onSubmit={onSubmitHandler}>
			<div className="form-group">
				<label htmlFor="fullname">Fullname</label>
				<input className="form-control" type="text" id="fullname" name="fullname" onChange={onChangeHandler}/>
			</div>
			<div className="form-group">
				<label htmlFor="username">Username</label>
				<input className="form-control" type="text" id="username" name="username" onChange={onChangeHandler}/>
			</div>
			<div className="form-group">
				<label htmlFor="password">Password</label>
				<input className="form-control" type="password" id="password" name="password" onChange={onChangeHandler}/>
			</div>
			<div className="form-group">
				<label htmlFor="password2">Confirm Password</label>
				<input className="form-control" type="password" id="password2" name="password2" onChange={onChangeHandler}/>
			</div>
			<button className="btn btn-success">Register</button>
		</form>
	)
}

export default Register