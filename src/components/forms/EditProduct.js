import React, { useState, useEffect } from "react"
import Swal from "sweetalert2"
import { URL } from "../../config"

const EditProduct = ({product, setEditing}) => {
	const [formData, setFormData] = useState(product)
	const [categories, setCategories] = useState([])
	useEffect (() => {
		fetch(`${URL}/categories`, {
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			setCategories(data)
		})

	}, [categories])

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}

	const handleFile = (e) => {
		// console.log(e.target.files[0])
		setFormData({
			...formData,
			image: e.target.files[0]
		})
	}

	const onSubmitHandler = (e) => {
		e.preventDefault()
		let updatedProduct = new FormData()
		updatedProduct.append('name', formData.name)
		updatedProduct.append('description', formData.description)
		updatedProduct.append('categoryId', formData.categoryId)
		updatedProduct.append('price', formData.price)
		if(typeof formData.image === 'object') {
			updatedProduct.append('image', formData.image)
		}

		fetch(`${URL}/products/${formData._id}`, {
			method: "PUT",
			body: updatedProduct,
			headers: {
				"x-auth-token": localStorage.getItem('token')
			}
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				icon: 'success',
				'text': data.message
			})
			setEditing(false)
		})
	}

	return (
		<form className="mx-auto" onSubmit={onSubmitHandler} encType="multipart/form-data">
			<div className="form-group">
				<label>Name</label>
				<input 
					type="text" 
					name="name" 
					className="form-control" 
					onChange={onChangeHandler} 
					value={formData.name}
				/>
			</div>
			<div className="form-group">
				<label>Description</label>
				<input 
					type="text" 
					name="description" 
					className="form-control" 
					onChange={onChangeHandler} 
					value={formData.description}
				/>
			</div>
			<div className="form-group">
				<label>Price</label>
				<input 
					type="number" 
					name="price" 
					className="form-control" 
					onChange={onChangeHandler} 
					value={formData.price}
				/>
			</div>
			<div className="form-group">
				<label>Image</label>
				<img src={`${URL}/${formData.image}`} className="img-fluid" alt="img"/>
				<input type="file" name="image" className="form-control" onChange={handleFile}/>
			</div>
			<select className="form-control mb-3" name="categoryId" onChange={onChangeHandler}>
				{categories.map(category => {
					return(
						category._id === formData.categoryId ?
						<option value={category._id} selected>{category.name}</option> :
						<option value={category._id} key={category._id}>{category.name}</option>
					)
				})}
			</select>
			<div className="text-center">
				<button className="btn btn-primary mx-2">Edit Product</button>
				<button className="btn btn-secondary mx-2" type="button" onClick={() => setEditing(false)}>Cancel</button>
			</div>
		</form>
	)
}

export default EditProduct