import React from "react"
import StripeCheckout from "react-stripe-checkout"
import { PUBLISHABLE_KEY, URL } from "../../config"

const Stripe = ({amount, cartItems, setCartItems}) => {
	const checkout = (token) => {
		let body = {
			token,
			amount,
			cartItems
		}
		fetch(`${URL}/transactions/stripe`, {
			method: "POST",
			body: JSON.stringify(body),
			headers: {
				"Content-Type": "application/json",
				"x-auth-token": localStorage.getItem('token')
			}
		})
		.then(res => res.json())
		.then(data => {
			localStorage.setItem('cartItems', JSON.stringify([]))
			// setCartItems(localStorage.getItem('cartItems'))
			window.location.href="/cart"
		})
	}

	return(
		<StripeCheckout 
			stripeKey={PUBLISHABLE_KEY}
			label="Card Payment"
			name="B49 Ecommerce"
			description="Happy to serve!"
			panelLabel="submit"
			amount={amount}
			billingAddress={false}
			currency="PHP"
			allowRememberMe={false}
			token={checkout}
		/>
	)
}

export default Stripe