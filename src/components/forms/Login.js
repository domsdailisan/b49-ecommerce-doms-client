import React, { useState } from "react"
import Swal from "sweetalert2"
import { URL } from "../../config"

const Login = () => {
	const [formData, setFormData] = useState({})

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}
	const onSubmitHandler = (e) => {
		e.preventDefault()
		// alert(JSON.stringify(formData))
		let reqOptions = {
			method: "POST",
			body: JSON.stringify(formData),
			headers: {
				"Content-Type": "application/json"
			}
		}
		fetch(`${URL}/users/login`, reqOptions)
		.then(res => res.json())
		.then(data => {
			if(data.auth) {
				Swal.fire({
					icon: 'success',
					title: data.message,
					showConfirmButton: false,
					timer: 3500
				})
				localStorage.setItem("user", JSON.stringify(data.user))
				localStorage.setItem("token", data.token)
				let cartItems = []
				localStorage.setItem("cartItems", JSON.stringify(cartItems))
				window.location.href="/"
			}else {
				Swal.fire({
					icon: 'error',
					title: data.message,
					showConfirmButton: false,
					timer: 2500,
					timerProgressBar: true
				})
			}
		})
	}

	return (
		<form className="mx-auto col-sm-6" onSubmit={onSubmitHandler}>
			<div className="form-group">
				<label htmlFor="username">Username</label>
				<input type="text" className="form-control" name="username" onChange={onChangeHandler} />
			</div>
			<div className="form-group">
				<label htmlFor="password">Password</label>
				<input type="password" className="form-control" name="password" onChange={onChangeHandler} />
			</div>
			<button className="btn btn-success">Login</button>
		</form>
	)
}

export default Login