import React, { useState } from "react"
import { 
	Container, Row, Col, Card, 
	CardTitle, CardText, Button, 
	CardImg
} from "reactstrap"
import { Link } from "react-router-dom"
import { URL } from "../config"

const Catalog = ({products, user, token}) => {
	const [quantity, setQuantity] = useState(0)

	const addToCartHandler = (e, product) => {
		// alert(e.target.previousElementSibling.value)
		let cartItems = JSON.parse(localStorage.getItem('cartItems'))
		
		// if(cartItems.length) {
		// 	let matched = cartItems.find(item => {
		// 		return item._id === product._id
		// 	})
		// 	if(matched){
		// 		let updatedCart = cartItems.map(item => {
		// 			return item._id === product._id ? 

		// 			{...item, quantity: item.quantity += parseInt(e.target.previousElementSibling.value)}: item
		// 		})
		// 		localStorage.setItem('cartItems', JSON.stringify(updatedCart))
		// 	}else {
		// 		cartItems.push({...product, quantity})
		// 	}
		// }else {
		// 	cartItems.push({...product, quantity}) 
		// }
		// localStorage.setItem('cartItems', JSON.stringify(cartItems))

		let matched = cartItems.find(item => {
			return item._id === product._id
		})

		if (matched) {
			matched.quantity += parseInt(e.target.previousElementSibling.value)
		}else {
			cartItems.push({
				...product,
				quantity: parseInt(e.target.previousElementSibling.value)
			})
		}
		localStorage.setItem('cartItems', JSON.stringify(cartItems))
	}

	const showProducts = products.map(product => (
		<Col sm="4" className="pt-2" key={product._id}>
			<Card className="p-2">
			{/*<CardImg src={"http://localhost:4000"+product.image}/>*/}
			<CardImg src={`${URL}${product.image}`}/>
				<CardTitle>
					<em>Title: </em><Link to={`/products/${product._id}`}>{product.name}</Link>
				</CardTitle>
				<CardText><em>Description: </em>{product.description}</CardText>
				<h6><em>Price:</em> {product.price}</h6>
				
				{ 
					user && token && user.isAdmin === false ?
					<div>
						<input type="number" min="1" className="form-control"
						onChange={(e) => setQuantity(parseInt(e.target.value))} defaultValue="1"/>
						<Button onClick={(e) => {
							addToCartHandler(e, product)
							e.target.previousElementSibling.value = "1"
						}}>
							Add to Cart
						</Button>
					</div> :
					null
				}
			</Card>
		</Col>
	))

	return (
		<Container>
			<h2 className="text-center">All Products</h2>
			<Row>
				{showProducts}
			</Row>
		</Container>
	)
}

export default Catalog;