import React, { useState, useEffect } from "react"
import { useParams } from "react-router-dom"
import { 
	Card, CardImg, Modal, ModalBody 
} from "reactstrap"
import Swal from "sweetalert2"

import EditProduct from "./forms/EditProduct"
import { URL } from "../config"

const Product = ({user, token}) => {
	let {id} = useParams()
	const [product, setProduct] = useState({})
	const [editing, setEditing] = useState(false)
	
	const [modal, setModal] = useState(false)
	const toggle = () => setModal(!modal)

	useEffect ( () => {
		fetch(`${URL}/products/${id}`)
		.then(res => res.json())
		.then(data => setProduct(data))
	}, [product, id])

	const deleteProduct = (productId) => {
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            fetch(+productId, {
                method: "DELETE",
                headers: {
                    "x-auth-token": token
                }
            })
            .then(res => res.json())
            .then(data => {
                Swal.fire(
                  'Deleted!',
                  `${data.message}`,
                  'success'
                )
                window.location.href="/"
            })
            
          }
        })
    }

	return(
		<div className="col-6 offset-3">
			<h2 className="text-center">Product Details</h2>
			<Card className="p-2 m-4">
				{
					editing ?
					<EditProduct product={product} setEditing={setEditing} /> :
					<div>
						<CardImg onClick={toggle} src={`${URL}${product.image}`}/>
						<h3><em>Title: </em>{product.name}</h3>
						<p><em>Description: </em>{product.description}</p>
						<h6><em>Price: </em>{product.price}</h6>
						<h6><em>Category:</em> {product.categoryId}</h6>
						{
							user && user.isAdmin && token ?
							<div className="text-center">
								<button className="btn-lg btn-warning mx-2" onClick={() => setEditing(true)}>Edit</button>
								<button className="btn-lg btn-danger mx-2" onClick={() => deleteProduct(product._id)}>Delete</button>
							</div> :
							null
						}
					</div>
				}
			</Card>

			<Modal isOpen={modal} size="lg" toggle={toggle}>
				<ModalBody>
					<CardImg src={`${URL}${product.image}`}/>
				</ModalBody>
			</Modal>


		</div>
	)
}

export default Product
